package com.jyggalag;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class DaysUntil {

    private static final DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public static void main(String... args) {
        Calendar cal = Calendar.getInstance();
        String dateStart = sdf.format(cal.getTime());

        Scanner in = new Scanner(System.in);

        String[] vals = new String[6];

        System.out.println("Podaj dzień: ");
        vals[0] = in.nextLine();
        System.out.println("Podaj miesiąc");
        vals[1] = in.nextLine();
        System.out.println("Podaj rok");
        vals[2] = in.nextLine();
        vals[3] = vals[4] = vals[5] = "00";

        String dateStop = vals[1]+"/"+vals[0]+"/"+vals[2]+" 00:00:00";

        System.out.println(dateStart);
        System.out.println(dateStop);

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = sdf.parse(dateStart);
            d2 = sdf.parse(dateStop);

            long diff = d2.getTime() - d1.getTime();

            long diffSecond = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if (diff >= 0) {
                System.out.println("=====\nPozostało: " + diffDays + " dni, " + diffHours + " godzin, "
                        + diffMinutes + " minut, " + diffSecond + " seknund");
            } else {
                System.out.println("=====\nMinęło: " + diffDays*(-1) + " dni, " + diffHours*(-1) + " godzin, "
                        + diffMinutes*(-1) + " minut, " + diffSecond*(-1) + " seknund");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}